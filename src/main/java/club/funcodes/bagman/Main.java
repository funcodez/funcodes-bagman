// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.bagman;

import static org.refcodes.cli.CliSugar.*;

import org.refcodes.archetype.CliHelper;
import org.refcodes.cli.Example;
import org.refcodes.cli.Flag;
import org.refcodes.cli.StringOption;
import org.refcodes.cli.Term;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;
import org.refcodes.properties.ext.application.ApplicationProperties;
import org.refcodes.textual.Font;
import org.refcodes.textual.FontFamily;
import org.refcodes.textual.FontStyle;

/**
 * A minimum REFCODES.ORG enabled command line interface (CLI) application. Get
 * inspired by "https://bitbucket.org/funcodez".
 */
public class Main {

	private static final RuntimeLogger LOGGER = RuntimeLoggerFactorySingleton.createRuntimeLogger();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String NAME = "BagMAN";
	private static final String TITLE = "(((" + NAME.toUpperCase() + ")))";
	private static final String DEFAULT_CONFIG = NAME + ".ini";
	private static final String LICENSE_NOTE = "Licensed under GNU General Public License, v3.0 and Apache License, v2.0";
	private static final String COPYRIGHT = "Copyright (c) by CLUB.FUNCODES (see [https://www.funcodes.club])";
	private static final char[] BANNER_PALETTE = AsciiColorPalette.MAX_LEVEL_GRAY.getPalette();
	private static final Font BANNER_FONT = new Font( FontFamily.DIALOG, FontStyle.BOLD );
	private static final String DESCRIPTION = "BagMAN goes round and round and rounf ... Get inspired by [https://bitbucket.org/funcodez].";

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	public static void main( String args[] ) {

		// ---------------------------------------------------------------------
		// CLI:
		// ---------------------------------------------------------------------

		final StringOption theEchoOption = stringOption( 'e', "echo", "echo", "Echoes the provided message to the standard out stream." );
		final Flag theVerboseFlag = verboseFlag();
		final Flag theSysInfoFlag = sysInfoFlag( false );
		final Flag theHelpFlag = helpFlag();

		// @formatter:off
		final Term theArgsSyntax = cases(
			and( 
				theEchoOption, any( theVerboseFlag ) 
			),
			xor(
				theHelpFlag, theSysInfoFlag
			)
		);
		final Example[] theExamples = examples(
				example( "To show the help text", theHelpFlag ),
				example( "To print the system info", theSysInfoFlag )
			);
			final CliHelper theCliHelper = CliHelper.builder().
				withArgs( args ).
				// withArgs( args, ArgsFilter.D_XX ).
				withArgsSyntax( theArgsSyntax ).
				withExamples( theExamples ).
				withFilePath( DEFAULT_CONFIG ). // Must be the name of the default (template) configuration file below "/src/main/resources"
				withResourceClass( Main.class ).
				withName( NAME ).
				withTitle( TITLE ).
				withDescription( DESCRIPTION ).
				withLicense( LICENSE_NOTE ).
				withCopyright( COPYRIGHT ).
				withBannerFont( BANNER_FONT ).
				withBannerFontPalette( BANNER_PALETTE ).
				withLogger( LOGGER ).build();
		// @formatter:on

		final ApplicationProperties theArgsProperties = theCliHelper.getApplicationProperties();

		// ---------------------------------------------------------------------
		// MAIN:
		// ---------------------------------------------------------------------

		final boolean isVerbose = theCliHelper.isVerbose();

		try {
			if ( isVerbose ) {
				for ( String eKey : theArgsProperties.sortedKeys() ) {
					LOGGER.info( eKey + "=" + theArgsProperties.get( eKey ) );
				}
			}

		}
		catch ( Throwable e ) {
			theCliHelper.exitOnException( e );
		}
	}
}